package com.spectrofinance.currencyutil;

import com.spectrofinance.model.Currency;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class CurrencyUtils {
    public static Currency convert(Currency fromCurrency, Currency toCurrency) {
        MathContext mc = new MathContext(100, RoundingMode.DOWN);
        BigDecimal monetaryValue = fromCurrency.getCurrencyExchangeRate()
                .divide(toCurrency.getCurrencyExchangeRate(), mc);
        return new Currency(monetaryValue, toCurrency.getCurrencyMnemonic());
    }
}
