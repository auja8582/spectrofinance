package com.spectrofinance.cvsutil;

import com.spectrofinance.model.Currency;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class CvsUtils {


    /**
     *  Map<String, Currency> currencies = new LinkedHashMap<>();
     *  for (String line : Files.readAllLines(inputFile.toPath())) {
     *      if (line.isEmpty() == false) {
     *          String mnemonic = line.split(",")[0].trim();
     *          BigDecimal exchangeRateToEur =
     *                  new BigDecimal(line.split(",")[1].trim());
     *          currencies.put(mnemonic,
     *                  new Currency(exchangeRateToEur, mnemonic));
     *      }
     * }
     */
    public static Map<String, Currency> parseFile(Path inputFile)
            throws IOException {
        return Files.lines(inputFile)
                .filter(Objects::nonNull)
                .map(s -> s.split(","))
                .filter(s -> s.length == 2)
                .collect(Collectors
                        .toMap(s -> s[0].trim(),
                                s -> new Currency(new BigDecimal(s[1].trim()),
                                        s[0].trim())));

    }

    /**
     *   List<Currency> currencies = new ArrayList<>();
     *   for (String line : Files.readAllLines(inputFile.toPath())) {
     *       if (line.isEmpty() == false) {
     *           String mnemonic = line.split(",")[0].trim();
     *           BigDecimal exchangeRateToEur =
     *                   new BigDecimal(line.split(",")[1].trim());
     *           currencies.add(new Currency(exchangeRateToEur, mnemonic));
     *       }
     *   }
     *
     *
     * */
    public static List<Currency> parseFile(Path inputFile, boolean list)
            throws IOException {
        return Files.lines(inputFile)
                .filter(Objects::nonNull)
                .map(s -> s.split(","))
                .filter(s -> s.length == 2)
                .map(s -> new Currency(new BigDecimal(s[1]), s[0]))
                .collect(Collectors.toList());
    }

}
