package com.spectrofinance.model;

import java.math.BigDecimal;

public class Currency {
    private BigDecimal currencyExchangeRate;

    /*TODO a very sinful way to represent a currency
     * supposed to be expressed as enum. Minus few karma points
     */
    private String currencyMnemonic;

    public Currency() {

    }

    public Currency(BigDecimal currencyExchangeRate,
                    String currencyMnemonic) {
        this.currencyExchangeRate = currencyExchangeRate;
        this.currencyMnemonic = currencyMnemonic;
    }

    public BigDecimal getCurrencyExchangeRate() {
        return currencyExchangeRate;
    }

    public void setCurrencyExchangeRate(
            BigDecimal currencyExchangeRate) {
        this.currencyExchangeRate = currencyExchangeRate;
    }

    public String getCurrencyMnemonic() {
        return currencyMnemonic;
    }

    public void setCurrencyMnemonic(String currencyMnemonic) {
        this.currencyMnemonic = currencyMnemonic;
    }
}
