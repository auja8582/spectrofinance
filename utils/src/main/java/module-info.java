module com.spectrofinance.utils {
    exports com.spectrofinance.currencyutil;
    exports com.spectrofinance.cvsutil;
    exports com.spectrofinance.testutil;
    exports com.spectrofinance.model;
}