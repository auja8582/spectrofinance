package com.spectrofinance.currencyutil;

import com.spectrofinance.cvsutil.CvsUtils;
import com.spectrofinance.model.Currency;
import com.spectrofinance.testutil.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CurrencyUtilsTest {

    @Parameterized.Parameters(name = "from {2} to {1} output: {0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new BigDecimal("0.710526315789473684"), "USD", "GBP"},
                {new BigDecimal("1.407407407407407407"), "GBP", "USD"},
                {new BigDecimal("6718.246913580246913580"), "BTC", "USD"},
                {new BigDecimal("5441.780000000000000000"), "BTC", "EUR"},
                {new BigDecimal("1.140000000000000000"), "GBP", "EUR"},
                {new BigDecimal("1.000000000000000000"), "EUR", "EUR"},

        });
    }

    private final BigDecimal result;
    private final String toCurrency;
    private final String fromCurrency;

    public CurrencyUtilsTest(BigDecimal result,
                             String fromCurrency,
                             String toCurrency) {

        this.result = result;
        this.toCurrency = toCurrency;
        this.fromCurrency = fromCurrency;
    }


    @Test
    public void convertTest() throws URISyntaxException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "dataForCurrencyUtilsTest.csv";
        Path inputTestFile =
                Paths.get(TestUtils.getAbsolutePath(inputFilePath, classLoader));
        Map<String, Currency> currencies = CvsUtils.parseFile(inputTestFile);

        assertEquals(result,
                CurrencyUtils
                        .convert(currencies.get(fromCurrency),
                                currencies.get(toCurrency))
                        .getCurrencyExchangeRate()
                        .setScale(18, RoundingMode.DOWN)
        );
    }

}
