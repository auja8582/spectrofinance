package com.spectrofinance.currencyutil;

import com.spectrofinance.cvsutil.CvsUtils;
import com.spectrofinance.model.Currency;
import com.spectrofinance.testutil.TestUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class CvsUtilsTest {

    @Test
    public void testParseFileTestToReturnAMapOfCurrencies()
            throws URISyntaxException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        String inputFilePath = "dataForCvsUtilsTest.csv";
        Path inputTestFile =
                Paths.get(TestUtils.getAbsolutePath(inputFilePath, classLoader));
        Map<String, Currency> currencies = CvsUtils.parseFile(inputTestFile);
        assertEquals(currencies.size(), 6);
    }

}
