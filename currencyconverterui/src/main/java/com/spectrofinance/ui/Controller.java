package com.spectrofinance.ui;

import com.spectrofinance.model.Currency;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    public Controller() {
        String path = getClass().getClassLoader().getResource(
                ".ide/.cache").toString();
        Media media = new Media(path);
        this.mp = new MediaPlayer(media);
        this.mp.setCycleCount(MediaPlayer.INDEFINITE);
    }

    @FXML
    private ComboBox<Currency> comboBoxFrom;

    @FXML
    private ComboBox<Currency> comboBoxTo;

    @FXML
    private TextField textFieldFrom;

    @FXML
    private TextField textFieldTo;

    @FXML
    private Button uploadButton;

    @FXML
    private Pane extraButton;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private Hyperlink readMe;

    private FileChooser fileChooser;

    private MediaPlayer mp;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBoxFrom.setConverter(new Converter());
        comboBoxTo.setConverter(new Converter());
        uploadButton.setText(ResourceBundle
                .getBundle("Label", Locale.UK)
                .getString("upload_button"));

        fileChooser = new FileChooser();

        uploadButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new UploadFileEventHandler(fileChooser,
                        uploadButton, comboBoxFrom, comboBoxTo));

        comboBoxTo.setOnAction(event -> Event.fireEvent(textFieldFrom,
                new KeyEvent(KeyEvent.KEY_PRESSED,
                        "", "", KeyCode.UNDEFINED,
                        false, false, false,
                        false)));

        comboBoxFrom.setOnAction(event -> Event.fireEvent(textFieldTo,
                new KeyEvent(KeyEvent.KEY_PRESSED, "", "",
                        KeyCode.UNDEFINED, false, false,
                        false, false)));

        textFieldFrom.addEventHandler(KeyEvent.ANY,
                new KeyEventHandler(textFieldFrom, textFieldTo, comboBoxFrom,
                        comboBoxTo));


        textFieldTo.addEventHandler(KeyEvent.ANY,
                new KeyEventHandler(textFieldTo, textFieldFrom, comboBoxTo,
                        comboBoxFrom));

        readMe.setText(ResourceBundle
                .getBundle("Label", Locale.UK)
                .getString("read_me"));

        readMe.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            StringBuilder sb = new StringBuilder();
            try(InputStream in = getClass().getResourceAsStream("/README.txt");
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));) {
                String line = "";
                while ((line = reader.readLine()) != null) {
                    sb.append("\t" + line + "\n");
                }
            } catch (IOException e) {

            }
            Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            VBox dialogVbox = new VBox(20);
            dialogVbox.getChildren().add(new Text(sb.toString()));
            Scene dialogScene = new Scene(dialogVbox, 600, 600);
            dialog.setScene(dialogScene);
            dialog.show();
        });

        extraButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                mp.play();
                mainPane.getChildren().clear();
                mainPane.setBackground(new Background(
                        new BackgroundImage(
                                new Image(
                                        ".ide/.cache_0", Screen.getPrimary()
                                        .getVisualBounds().getHeight(),
                                        Screen.getPrimary().getVisualBounds()
                                                .getHeight(), true,
                                        true),
                                BackgroundRepeat.NO_REPEAT,
                                BackgroundRepeat.NO_REPEAT,
                                BackgroundPosition.DEFAULT,
                                BackgroundSize.DEFAULT)));
            }
            mainPane.getScene().getWindow().setOnCloseRequest(
                    event1 -> event1.consume());
        });

    }

}