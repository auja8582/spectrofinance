package com.spectrofinance.ui;

import com.spectrofinance.model.Currency;
import javafx.util.StringConverter;

public class Converter extends StringConverter<Currency> {
    @Override
    public String toString(Currency object) {
        if (object == null)
            return "";
        return object.getCurrencyMnemonic();
    }

    @Override
    public Currency fromString(String string) {
        return null;
    }
}
