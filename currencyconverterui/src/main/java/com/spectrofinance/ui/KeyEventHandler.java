package com.spectrofinance.ui;

import com.spectrofinance.currencyutil.CurrencyUtils;
import com.spectrofinance.model.Currency;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class KeyEventHandler implements EventHandler<Event> {
    TextField textFieldTo;
    TextField textFieldFrom;
    ComboBox<Currency> comboBoxTo;
    ComboBox<Currency> comboBoxFrom;

    public KeyEventHandler(TextField textFieldTo, TextField textFieldFrom,
                           ComboBox<Currency> comboBoxTo,
                           ComboBox<Currency> comboBoxFrom) {
        this.textFieldTo = textFieldTo;
        this.textFieldFrom = textFieldFrom;
        this.comboBoxTo = comboBoxTo;
        this.comboBoxFrom = comboBoxFrom;
    }

    @Override
    public void handle(Event event) {
        handle(textFieldTo, textFieldFrom, comboBoxTo, comboBoxFrom);
    }

    public void handle(TextField textFieldTo, TextField textFieldFrom,
                       ComboBox<Currency> comboBoxTo,
                       ComboBox<Currency> comboBoxFrom) {
        if (textFieldTo.getText() != null && textFieldTo.getText()
                .matches("^\\d+(\\.?\\d+)?$")) {
            if (comboBoxTo.getSelectionModel() != null
                    && comboBoxTo.getSelectionModel().getSelectedItem() != null
                    && comboBoxFrom.getSelectionModel() != null
                    && comboBoxFrom.getSelectionModel()
                    .getSelectedItem() != null) {

                Currency convertedValue = CurrencyUtils.convert(
                        comboBoxTo.getSelectionModel().getSelectedItem(),
                        comboBoxFrom.getSelectionModel().getSelectedItem()
                );

                textFieldFrom.setText(convertedValue.getCurrencyExchangeRate()
                        .multiply(new BigDecimal(textFieldTo.getText()))
                        .setScale(18, RoundingMode.DOWN)
                        .toPlainString());
            }
        } else {
            textFieldFrom.setText("");
        }
    }
}
