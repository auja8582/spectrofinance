package com.spectrofinance.ui;

import com.spectrofinance.cvsutil.CvsUtils;
import com.spectrofinance.model.Currency;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class UploadFileEventHandler implements EventHandler<Event> {

    public UploadFileEventHandler(FileChooser fileChooser,
                                  Button uploadButton,
                                  ComboBox<Currency> comboBoxFrom,
                                  ComboBox<Currency> comboBoxTo) {
        this.fileChooser = fileChooser;
        this.uploadButton = uploadButton;
        this.comboBoxFrom = comboBoxFrom;
        this.comboBoxTo = comboBoxTo;
    }

    private FileChooser fileChooser;
    private Button uploadButton;
    private ComboBox<Currency> comboBoxFrom;
    private ComboBox<Currency> comboBoxTo;
    private List<Currency> currencies;

    public void handle(FileChooser fileChooser, Button uploadButton,
                       ComboBox<Currency> comboBoxFrom,
                       ComboBox<Currency> comboBoxTo) {
        Path file = fileChooser.showOpenDialog(
                uploadButton.getScene().getWindow()
        ).toPath();
        if (file != null) {
            try {
                currencies = CvsUtils.parseFile(file, true);
            } catch (IOException e) {

            }
            if (currencies != null && currencies.isEmpty() == false) {
                comboBoxFrom.setItems(
                        FXCollections.observableArrayList(currencies)
                );
                comboBoxTo.setItems(
                        FXCollections.observableArrayList(currencies)
                );
            }
            comboBoxTo.getSelectionModel().selectFirst();
            comboBoxFrom.getSelectionModel().selectFirst();

        }
    }

    @Override
    public void handle(Event event) {
        handle(fileChooser, uploadButton, comboBoxFrom, comboBoxTo);
    }
}
