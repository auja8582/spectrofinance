module com.spectrofinance.ui{
    requires com.spectrofinance.utils;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    opens com.spectrofinance.ui to javafx.graphics, javafx.fxml, javafx.media, javafx.controls ;


}