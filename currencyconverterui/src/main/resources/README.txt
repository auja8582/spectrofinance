PREFACE:
----------------------------------------------

This application is for converting currency from one to another.

FILE FORMAT TO UPLOAD:
-----------------------------------------------

file is csv file and follows a format i.e

EUR,1.000000000000000000
USD,0.809552722
GBP,1.126695
BTC,5710.26
ETH,685.29447470022
FKE,0.0250

ADDITIONAL NOTES:
------------------------------------------------
not everything was done i.e

1. No logging was used
2. TDD was used only on currency converter module
3. Testing was skipped on ui (user interface), as it is not a main functionality.

WARNINGS:
-------------------------------------------------
4. DO NOT PRESS THAT BUTTON!!!!!